require open formalization_root.Matching_Logic.Typing;
require open formalization_root.Matching_Logic.Constructor;
require open formalization_root.Matching_Logic.Notation;
require open formalization_root.Matching_Logic.Proof_system;

// ⇒_ML is equivalent to →

symbol ⇒_MLto→ : Π (φ1 φ2 : #Pattern),
  Prf ( φ1 ⇒_ML φ2 ) → (Prf φ1 → Prf φ2) ≔
  λ φ1 φ2 hyp Hφ1, mp φ1 φ2 Hφ1 hyp ;

type λ φ1 φ2 hyp Hφ1, mp φ1 φ2 Hφ1 hyp ;

symbol ⇒_MLto→bis : Π (φ1 φ2 : #Pattern),
  Prf ( φ1 ⇒_ML φ2 ) → (Prf φ1 → Prf φ2) ≔
  λ φ1 φ2 hyp,
    λ Hφ1, mp φ1 φ2 Hφ1 hyp ;
// Same !

rule Prf ($φ0 ⇒_ML $φ1) ↪ Prf $φ0 → Prf $φ1;


symbol →to⇒_ML : Π (φ1 φ2 : #Pattern),
  Prf φ1 → Prf φ2 → Prf ( φ1 ⇒_ML φ2 ) ≔
  λ φ1 φ2 _ hyp, mp φ2 (φ1 ⇒_ML φ2) hyp (prop-1 φ2 φ1) ;

symbol →to⇒_MLbis : Π (φ1 φ2 : #Pattern),
  (Prf φ1 → Prf φ2) → Prf ( φ1 ⇒_ML φ2 ) ≔
  λ φ1 φ2 hyp, mp φ2 (φ1 ⇒_ML φ2) hyp (prop-1 φ2 φ1) ;


type λ φ1 φ2 hyp, mp φ2 (φ1 ⇒_ML φ2) hyp (prop-1 φ2 φ1) ;

type λ φ1 φ2, prop-1 φ2 φ1 ;

type λ (φ1 φ2 : #Pattern) (hyp : Prf φ2),
     mp φ2 (φ1 ⇒_ML φ2) hyp (prop-1 φ2 φ1) ;
